package pe.edu.pucp.examen1.news.presenter;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONObject;

import pe.edu.pucp.examen1.common.ExAPI;
import pe.edu.pucp.examen1.news.model.News;
import pe.edu.pucp.examen1.news.view.INewsView;

public class NewsPresenter implements  INewsPresenter {
    private INewsView view;

    public NewsPresenter(INewsView view) {
        this.view = view;
    }

    @Override
    public void getCurrentNews() {
        ExAPI.getInstance(view.getActivityContext())
                .netNews(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        News localNews = jsonToNewsObject(response);
                        view.drawNews(localNews);
                    }

                    @Override
                    public void onError(ANError anError) {
                        view.drawErrorNewsInformation(anError.toString());
                    }
                });
    }

    @Override
    public News jsonToNewsObject(JSONObject object) {
        JSONObject payload = object.optJSONObject("payload");
        if (payload == null) {
            return null;
        }

        News news = new News();

        news.setTitle(payload.optString("title"));
        news.setContent(payload.optString("content"));
        news.setComments(payload.optString("comments"));
        news.setVisualizations(payload.optString("visualizations"));
        news.setLikes(payload.optString("likes"));

        return news;
    }
}
