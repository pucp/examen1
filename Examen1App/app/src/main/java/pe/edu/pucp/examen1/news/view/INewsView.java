package pe.edu.pucp.examen1.news.view;

import android.content.Context;

import pe.edu.pucp.examen1.news.model.News;

public interface INewsView {
    Context getActivityContext();
    void drawNews(News news);
    void drawErrorNewsInformation(String errorMessage);

    void setupFields();
}
