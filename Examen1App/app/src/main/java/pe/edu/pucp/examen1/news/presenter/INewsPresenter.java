package pe.edu.pucp.examen1.news.presenter;

import org.json.JSONObject;

import pe.edu.pucp.examen1.news.model.News;

public interface INewsPresenter {
    void getCurrentNews();
    News jsonToNewsObject(JSONObject object);
}
