package pe.edu.pucp.examen1.news.model;

public class News {
    private String comments;
    private String content;
    private String date;
    private String likes;
    private String title;
    private String visualizations;

    public News() {}

    public News(String comments, String content, String date, String likes, String title, String visualizations) {
        this.comments = comments;
        this.content = content;
        this.date = date;
        this.likes = likes;
        this.title = title;
        this.visualizations = visualizations;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVisualizations() {
        return visualizations;
    }

    public void setVisualizations(String visualizations) {
        this.visualizations = visualizations;
    }
}
