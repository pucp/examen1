package pe.edu.pucp.examen1.profile.presenter;

import org.json.JSONObject;

import pe.edu.pucp.examen1.profile.model.User;

public interface IProfilePresenter {
    void getUserProfile(String email);
    User jsonToUserObject(JSONObject object);
    JSONObject userObjectToJson(User user);
    void setUserProfile(User user);
}
