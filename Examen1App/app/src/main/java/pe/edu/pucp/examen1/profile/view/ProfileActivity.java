package pe.edu.pucp.examen1.profile.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.ImageView;

import pe.edu.pucp.examen1.R;
import pe.edu.pucp.examen1.news.view.NewsActivity;
import pe.edu.pucp.examen1.profile.model.User;
import pe.edu.pucp.examen1.profile.presenter.IProfilePresenter;
import pe.edu.pucp.examen1.profile.presenter.ProfilePresenter;

public class ProfileActivity extends AppCompatActivity implements IProfileView {

    private IProfilePresenter presenter;

    private EditText mProfileName, mProfileEmail, mProfilePhone, mProfileRole, mProfileCollege;
    private ImageView mProfileCheck, mProfileEdit;

    private boolean isEnabledEdition = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        this.presenter = new ProfilePresenter(this);
        this.setupFields();
        this.setupBehaviors();

        this.presenter.getUserProfile("d.ritchie@pucp.edu.pe");

    }

    @Override
    public Context getActivityContext() {
        return getApplicationContext();
    }

    @Override
    public void drawProfileInformation(User user) {
        mProfileName.setText(user.getName());
        mProfileEmail.setText(user.getEmail());
        mProfilePhone.setText(user.getPhone());
        mProfileRole.setText(user.getRole());
        mProfileCollege.setText(user.getCollege());
    }

    @Override
    public void drawErrorProfileInformation(String errorMessage) {
        // TODO: Implement a way to show the error that is coming from server
    }

    @Override
    public void setupFields() {
        mProfileName = findViewById(R.id.profile_name);
        mProfileEmail = findViewById(R.id.profile_email);
        mProfilePhone = findViewById(R.id.profile_phone);
        mProfileRole = findViewById(R.id.profile_role);
        mProfileCollege = findViewById(R.id.profile_college);

        mProfileCheck = findViewById(R.id.profile_check);
        mProfileEdit= findViewById(R.id.profile_edit);
    }

    @Override
    public void setupBehaviors() {
        mProfileCheck.setOnClickListener(v -> {
            if (isEnabledEdition) {
                User localUser = new User(mProfileName.getText().toString(),
                        mProfileEmail.getText().toString(),
                        mProfilePhone.getText().toString(),
                        mProfileRole.getText().toString(),
                        mProfileCollege.getText().toString());
                presenter.setUserProfile(localUser);
            } else {
                Intent intent = new Intent(ProfileActivity.this, NewsActivity.class);
                startActivity(intent);
            }
        });
        mProfileEdit.setOnClickListener(v -> toggleEnabledFields());
    }

    @Override
    public void toggleEnabledFields() {
        isEnabledEdition = !isEnabledEdition;
        mProfileName.setEnabled(isEnabledEdition);
        mProfileEmail.setEnabled(isEnabledEdition);
        mProfilePhone.setEnabled(isEnabledEdition);
        mProfileRole.setEnabled(isEnabledEdition);
        mProfileCollege.setEnabled(isEnabledEdition);
    }

}
