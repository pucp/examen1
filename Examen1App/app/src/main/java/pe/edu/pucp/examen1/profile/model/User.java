package pe.edu.pucp.examen1.profile.model;

public class User {
    private String name;
    private String email;
    private String phone;
    private String college;
    private String role;

    public User() {
    }

    public User(String name, String email, String phone, String college, String role) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.college = college;
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
