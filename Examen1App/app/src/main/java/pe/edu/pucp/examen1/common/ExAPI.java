package pe.edu.pucp.examen1.common;

import android.content.Context;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONObject;

public class ExAPI {

    private String signup = "http://demo8075177.mockable.io/signup";
    private String profile = "http://demo8075177.mockable.io/profile";
    private String news = "http://demo8075177.mockable.io/news";

    private Context mContext;

    private static ExAPI instance = new ExAPI();

    private ExAPI() {}

    public static ExAPI getInstance(Context context) {
        if(instance.mContext == null) {
            instance.mContext = context;
            AndroidNetworking.initialize(context);
        }
        return instance;
    }

    public void signup(JSONObjectRequestListener listener) {

        AndroidNetworking.post(signup)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(listener);
    }

    public void updateProfile(JSONObject user, JSONObjectRequestListener listener) {

        AndroidNetworking.patch(profile)
                .addJSONObjectBody(user)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(listener);
    }
    public void netNews(JSONObjectRequestListener listener) {

        AndroidNetworking.get(news)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(listener);
    }
}
