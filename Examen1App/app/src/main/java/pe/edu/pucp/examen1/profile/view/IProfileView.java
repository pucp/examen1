package pe.edu.pucp.examen1.profile.view;

import android.content.Context;

import pe.edu.pucp.examen1.profile.model.User;

public interface IProfileView {
    Context getActivityContext();
    void drawProfileInformation(User user);
    void drawErrorProfileInformation(String errorMessage);

    void setupFields();
    void setupBehaviors();

    void toggleEnabledFields();
}
