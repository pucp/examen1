package pe.edu.pucp.examen1.news.view;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import pe.edu.pucp.examen1.R;
import pe.edu.pucp.examen1.news.model.News;
import pe.edu.pucp.examen1.news.presenter.INewsPresenter;
import pe.edu.pucp.examen1.news.presenter.NewsPresenter;

public class NewsActivity extends AppCompatActivity implements  INewsView {

    private TextView mNewsTitle, mNewsContent, mNewsComments, mNewsViews, mNewsLikes;
    private INewsPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        presenter = new NewsPresenter(this);
        setupFields();

        presenter.getCurrentNews();
    }

    @Override
    public Context getActivityContext() {
        return getApplicationContext();
    }

    @Override
    public void drawNews(News news) {
        mNewsTitle.setText(news.getTitle());
        mNewsContent.setText(news.getContent());
        mNewsComments.setText(news.getComments());
        mNewsViews.setText(news.getVisualizations());
        mNewsLikes.setText(news.getLikes());
    }

    @Override
    public void drawErrorNewsInformation(String errorMessage) {
        // TODO: Implement a way to show the error that is coming from server
    }

    @Override
    public void setupFields() {
        mNewsTitle = findViewById(R.id.news_title);
        mNewsContent = findViewById(R.id.news_content);
        mNewsComments = findViewById(R.id.news_comments);
        mNewsViews = findViewById(R.id.news_views);
        mNewsLikes = findViewById(R.id.news_likes);
    }
}
