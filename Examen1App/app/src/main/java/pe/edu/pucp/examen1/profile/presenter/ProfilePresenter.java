package pe.edu.pucp.examen1.profile.presenter;

import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import pe.edu.pucp.examen1.common.ExAPI;
import pe.edu.pucp.examen1.profile.model.User;
import pe.edu.pucp.examen1.profile.view.IProfileView;

public class ProfilePresenter implements IProfilePresenter{
    private IProfileView view;

    public ProfilePresenter(IProfileView view) {
        this.view = view;
    }

    @Override
    public void getUserProfile(String email) {
        ExAPI.getInstance(view.getActivityContext())
                .signup(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        view.drawProfileInformation(jsonToUserObject(response));
                    }

                    @Override
                    public void onError(ANError anError) {
                        view.drawErrorProfileInformation(anError.toString());
                    }
                });
    }

    @Override
    public User jsonToUserObject(JSONObject object) {
        JSONObject payload = object.optJSONObject("payload");
        if (payload == null) {
            return null;
        }

        User user = new User();
        user.setName(payload.optString("name") + " " + payload.optString("lastname"));
        user.setEmail(payload.optString("mail"));
        user.setPhone(payload.optString("phone"));
        user.setRole(payload.optString("role"));
        user.setCollege(payload.optString("university"));

        return user;
    }

    @Override
    public JSONObject userObjectToJson(User user) {
        Gson gson = new Gson();
        String jsonString = gson.toJson(user);
        JSONObject request = new JSONObject();
        try {
            request = new JSONObject(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return request;

    }

    @Override
    public void setUserProfile(User user) {
        ExAPI.getInstance(view.getActivityContext())
                .updateProfile(userObjectToJson(user), new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        view.toggleEnabledFields();
                    }

                    @Override
                    public void onError(ANError anError) {
                        view.drawErrorProfileInformation(anError.toString());
                    }
                });
    }
}
